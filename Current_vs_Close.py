# -*- coding: utf-8 -*-
"""
Created on Wed Mar 31 14:34:41 2021

@author: Jorden.Wilder
"""

import numpy as np
import pandas as pd
import os
from datetime import datetime, time


#Convert our current vs close file into a dataframe
cc_df = pd.read_csv("Current_vs_Close.csv",  engine='python', index_col=0, na_values = ['no info', '.'])

#Conver mirador files into dataframes
fx_df = pd.read_csv("Mirador_FXSnapshot.csv", engine='python', index_col=0, na_values = ['no info', '.'] )
mtl_df = pd.read_csv("Mirador_MetalsSnapshot.csv", engine='python', index_col=0, na_values = ['no info', '.'])
idx_df = pd.read_csv("Mirador_IndexSnapshot.csv", engine='python', index_col=0, na_values = ['no info', '.'])
smry_df = pd.read_csv("Mirador_SummarySnapshot.csv", engine='python', index_col=0, na_values = ['no info', '.'])


#Main



#Gold
cc_df.iloc[cc_df.index.get_loc('Gold '),cc_df.columns.get_loc('Current  Client')] = mtl_df.iloc[mtl_df.index.get_loc('Gold'),mtl_df.columns.get_loc('Client')]
cc_df.iloc[cc_df.index.get_loc('Gold '),cc_df.columns.get_loc('Current  net')] = mtl_df.iloc[mtl_df.index.get_loc('Gold'),mtl_df.columns.get_loc('Net')]
cc_df.iloc[cc_df.index.get_loc('Gold '),cc_df.columns.get_loc('Delta client')] = mtl_df.iloc[mtl_df.index.get_loc('Gold'),mtl_df.columns.get_loc('Client.1')]
cc_df.iloc[cc_df.index.get_loc('Gold '),cc_df.columns.get_loc('Delta net ')] = mtl_df.iloc[mtl_df.index.get_loc('Gold'),mtl_df.columns.get_loc('Net.1')]
cc_df.iloc[cc_df.index.get_loc('Gold '),cc_df.columns.get_loc('Current Pl')] = mtl_df.iloc[mtl_df.index.get_loc('Gold'),mtl_df.columns.get_loc('Total')]


#Silver
cc_df.iloc[cc_df.index.get_loc('Silver '),cc_df.columns.get_loc('Current  Client')] = mtl_df.iloc[mtl_df.index.get_loc('Silver'),mtl_df.columns.get_loc('Client')]
cc_df.iloc[cc_df.index.get_loc('Silver '),cc_df.columns.get_loc('Current  net')] = mtl_df.iloc[mtl_df.index.get_loc('Silver'),mtl_df.columns.get_loc('Net')]
cc_df.iloc[cc_df.index.get_loc('Silver '),cc_df.columns.get_loc('Delta client')] = mtl_df.iloc[mtl_df.index.get_loc('Silver'),mtl_df.columns.get_loc('Client.1')]
cc_df.iloc[cc_df.index.get_loc('Silver '),cc_df.columns.get_loc('Delta net ')] = mtl_df.iloc[mtl_df.index.get_loc('Silver'),mtl_df.columns.get_loc('Net.1')]
cc_df.iloc[cc_df.index.get_loc('Silver '),cc_df.columns.get_loc('Current Pl')] = mtl_df.iloc[mtl_df.index.get_loc('Silver'),mtl_df.columns.get_loc('Total')]

#Index
cc_df.iloc[cc_df.index.get_loc('Index '),cc_df.columns.get_loc('Current  Client')] = smry_df.iloc[smry_df.index.get_loc('Index'),smry_df.columns.get_loc('Client')]
cc_df.iloc[cc_df.index.get_loc('Index '),cc_df.columns.get_loc('Current  net')] = smry_df.iloc[smry_df.index.get_loc('Index'),smry_df.columns.get_loc('Net')]
cc_df.iloc[cc_df.index.get_loc('Index '),cc_df.columns.get_loc('Delta client')] = smry_df.iloc[smry_df.index.get_loc('Index'),smry_df.columns.get_loc('Client.1')]
cc_df.iloc[cc_df.index.get_loc('Index '),cc_df.columns.get_loc('Delta net ')] = smry_df.iloc[smry_df.index.get_loc('Index'),smry_df.columns.get_loc('Net.1')]
cc_df.iloc[cc_df.index.get_loc('Index '),cc_df.columns.get_loc('Current Pl')] = smry_df.iloc[smry_df.index.get_loc('Index'),smry_df.columns.get_loc('Total')]

#Dax
cc_df.iloc[cc_df.index.get_loc('DAX'),cc_df.columns.get_loc('Current  Client')] = idx_df.iloc[idx_df.index.get_loc('Germany 30'),idx_df.columns.get_loc('Client')]
cc_df.iloc[cc_df.index.get_loc('DAX'),cc_df.columns.get_loc('Current  net')] = idx_df.iloc[idx_df.index.get_loc('Germany 30'),idx_df.columns.get_loc('Net')]
cc_df.iloc[cc_df.index.get_loc('DAX'),cc_df.columns.get_loc('Delta client')] = idx_df.iloc[idx_df.index.get_loc('Germany 30'),idx_df.columns.get_loc('Client.1')]
cc_df.iloc[cc_df.index.get_loc('DAX'),cc_df.columns.get_loc('Delta net ')] = idx_df.iloc[idx_df.index.get_loc('Germany 30'),idx_df.columns.get_loc('Net.1')]
cc_df.iloc[cc_df.index.get_loc('DAX'),cc_df.columns.get_loc('Current Pl')] = idx_df.iloc[idx_df.index.get_loc('Germany 30'),idx_df.columns.get_loc('Total')]

#Dow
cc_df.iloc[cc_df.index.get_loc('DOW'),cc_df.columns.get_loc('Current  Client')] = idx_df.iloc[idx_df.index.get_loc('Wall Street'),idx_df.columns.get_loc('Client')]
cc_df.iloc[cc_df.index.get_loc('DOW'),cc_df.columns.get_loc('Current  net')] = idx_df.iloc[idx_df.index.get_loc('Wall Street'),idx_df.columns.get_loc('Net')]
cc_df.iloc[cc_df.index.get_loc('DOW'),cc_df.columns.get_loc('Delta client')] = idx_df.iloc[idx_df.index.get_loc('Wall Street'),idx_df.columns.get_loc('Client.1')]
cc_df.iloc[cc_df.index.get_loc('DOW'),cc_df.columns.get_loc('Delta net ')] = idx_df.iloc[idx_df.index.get_loc('Wall Street'),idx_df.columns.get_loc('Net.1')]
cc_df.iloc[cc_df.index.get_loc('DOW'),cc_df.columns.get_loc('Current Pl')] = idx_df.iloc[idx_df.index.get_loc('Wall Street'),idx_df.columns.get_loc('Total')]

#FX
cc_df.iloc[cc_df.index.get_loc('FX'),cc_df.columns.get_loc('Current  Client')] = smry_df.iloc[smry_df.index.get_loc('FX'),smry_df.columns.get_loc('Client')]
cc_df.iloc[cc_df.index.get_loc('FX'),cc_df.columns.get_loc('Current  net')] = smry_df.iloc[smry_df.index.get_loc('FX'),smry_df.columns.get_loc('Net')]
cc_df.iloc[cc_df.index.get_loc('FX'),cc_df.columns.get_loc('Delta client')] = smry_df.iloc[smry_df.index.get_loc('FX'),smry_df.columns.get_loc('Client.1')]
cc_df.iloc[cc_df.index.get_loc('FX'),cc_df.columns.get_loc('Delta net ')] = smry_df.iloc[smry_df.index.get_loc('FX'),smry_df.columns.get_loc('Net.1')]
cc_df.iloc[cc_df.index.get_loc('FX'),cc_df.columns.get_loc('Current Pl')] = smry_df.iloc[smry_df.index.get_loc('FX'),smry_df.columns.get_loc('Total')]

#JPY
cc_df.iloc[cc_df.index.get_loc('JPY'),cc_df.columns.get_loc('Current  Client')] = fx_df.iloc[fx_df.index.get_loc('JPY'),fx_df.columns.get_loc('Client')]
cc_df.iloc[cc_df.index.get_loc('JPY'),cc_df.columns.get_loc('Current  net')] = fx_df.iloc[fx_df.index.get_loc('JPY'),fx_df.columns.get_loc('Net')]
cc_df.iloc[cc_df.index.get_loc('JPY'),cc_df.columns.get_loc('Delta client')] = fx_df.iloc[fx_df.index.get_loc('JPY'),fx_df.columns.get_loc('Client.1')]
cc_df.iloc[cc_df.index.get_loc('JPY'),cc_df.columns.get_loc('Delta net ')] = fx_df.iloc[fx_df.index.get_loc('JPY'),fx_df.columns.get_loc('Net.1')]
cc_df.iloc[cc_df.index.get_loc('JPY'),cc_df.columns.get_loc('Current Pl')] = fx_df.iloc[fx_df.index.get_loc('JPY'),fx_df.columns.get_loc('Total')]

#EUR
cc_df.iloc[cc_df.index.get_loc('EUR'),cc_df.columns.get_loc('Current  Client')] = fx_df.iloc[fx_df.index.get_loc('EUR'),fx_df.columns.get_loc('Client')]
cc_df.iloc[cc_df.index.get_loc('EUR'),cc_df.columns.get_loc('Current  net')] = fx_df.iloc[fx_df.index.get_loc('EUR'),fx_df.columns.get_loc('Net')]
cc_df.iloc[cc_df.index.get_loc('EUR'),cc_df.columns.get_loc('Delta client')] = fx_df.iloc[fx_df.index.get_loc('EUR'),fx_df.columns.get_loc('Client.1')]
cc_df.iloc[cc_df.index.get_loc('EUR'),cc_df.columns.get_loc('Delta net ')] = fx_df.iloc[fx_df.index.get_loc('EUR'),fx_df.columns.get_loc('Net.1')]
cc_df.iloc[cc_df.index.get_loc('EUR'),cc_df.columns.get_loc('Current Pl')] = fx_df.iloc[fx_df.index.get_loc('EUR'),fx_df.columns.get_loc('Total')]

#5PM calculations

#Convert data to float
cc_df['Current  Client'] = cc_df['Current  Client'].astype(float)
cc_df['Current  net'] = cc_df['Current  net'].astype(float)
cc_df['Current Price'] = cc_df['Current Price'].astype(float)
cc_df['5 pm Price'] = cc_df['5 pm Price'].astype(float)

#Do calculations for 5pm values
for x in range(len(cc_df.index)):
    cc_df.iloc[x,cc_df.columns.get_loc('5 pm  Client')] =  cc_df.iloc[x,cc_df.columns.get_loc('Current  Client')] - cc_df.iloc[x,cc_df.columns.get_loc('Delta client')]
    cc_df.iloc[x,cc_df.columns.get_loc('5 pm  net')] =  cc_df.iloc[x,cc_df.columns.get_loc('Current  net')] - cc_df.iloc[x,cc_df.columns.get_loc('Delta net ')]
    cc_df.iloc[x,cc_df.columns.get_loc('Price delta')] =  cc_df.iloc[x,cc_df.columns.get_loc('Current Price')] - cc_df.iloc[x,cc_df.columns.get_loc('5 pm Price')]
    
#Total
smry_df['Total'] = smry_df['Total'].astype(float)
cc_df.iloc[cc_df.index.get_loc('Total pl'),cc_df.columns.get_loc('Current Pl')] = sum(smry_df['Total'])

cc_df.to_csv('Final_Current_vs_Close.csv')



